#!/bin/bash
#Ejercicio 1 -- Creamos la carpeta
mkdir carpeta

#Ejercicio 2 -- inicializamos la carpeta
cd carpeta
git init
git remote add origin https://gitlab.com/InoueBI/controln2.git

#Ejercicio 3 -- Creo actualizacion
git pull origin master

#Ejercicio 4 -- 10 archivos con for
for i in {1..10}
do
	touch texto$i.txt
	echo "Bienvenido a mi texto" > texto$i.txt
done

#Ejercicio 5
git add .

#Ejercicio 6
git commit -a -m "Actualizamos los archivos"

#Ejercicio 7
git push origin master
